# media-analysis-suriya.skt

_**Analysis of media views and action ratings tables**_

**Files in the project:**
- media_analysis.ipynb			 - for developers use
- media_analysis_reporting.ipynb - to get analysis's results easily (same as media_analysis.ipynb with lesser cells. Suggested for monthly reporting)
- user_action_rating			 - holds action ratings details 'mc_public.mc_ux_action_rating' table from the database
- user_media_view 				 - holds media view details 'mc_public.useract_media_view' table from the database
- get_all_users 				 - holds details of all users 'mc_public.mc_user' from the database
- get_all_suggestion_resource 	 - holds details of the suggestion resources 'mc_public.suggestion_resource' table from the database

	
**NOTE** - Always retrieve the latest table from the database for current analysis

**Suggested way to run the above '.ipynb' files:**
1. Download and install Jupyter Notebook
1. Clone the project to your local machine
1. Place the ipynb and data files in the directory (make sure to change the directory details in the ipynb files before executing)
1. Execute every cell from the beginning
1. Comments are given at most of the lines
